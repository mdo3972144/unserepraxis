import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/kontakt',
      name: 'kontakt',
      component: () => import('../views/KontaktView.vue')
    },
    {
      path: '/kosten',
      name: 'kosten',
      component: () => import('../views/KostenView.vue')
    },
    {
      path: '/leistungen',
      name: 'leistungen',
      component: () => import('../views/LeistungenView.vue')
    }
  ]
})

export default router
